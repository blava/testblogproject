<?php

namespace App\Entity;

use App\Repository\BlogRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BlogRepository::class)]
class Blog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $blogText = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlogText(): ?string
    {
        return $this->blogText;
    }

    public function setBlogText(string $blogText): self
    {
        $this->blogText = $blogText;

        return $this;
    }
}
