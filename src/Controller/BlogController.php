<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use App\Entity\Blog;
use App\Service\EmailService;


class BlogController extends AbstractController
{
    public function __construct(private BlogRepository $repository,
                                private HtmlSanitizerInterface $htmlSanitizer,
                                private EntityManagerInterface $entityManager
                                ) {

    }


    #[Route('/blog', name: 'app_blog', methods : 'get')]
    public function index(): Response
    {
        $form = $this->createForm(BlogType::class);
        $data = $this->repository->findAll();
        return $this->render('blog/index.html.twig', [
            'form' =>$form,
            'data' => $data
        ]);
    }

    #[Route('/blog', name: 'create_blog', methods:'post')]
    public function create(Request $request, EmailService $emailService) {
        $form = $this->createForm(BlogType::class);

        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $blog = new Blog();
            $blog->setBlogText($form->get('blogText')->getData());
            $this->entityManager->persist($blog);
            $this->entityManager->flush();
            $emailService->sendEmail('Your Blog', 'somerecepient@example.com', $blog->getBlogText());
            return $this->redirectToRoute('app_blog');
        }
    }
   
}
