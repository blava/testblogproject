<?php


namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService
{

    public function __construct(private MailerInterface $mailer)
    {
    }

    public function sendEmail(string $subject, string $recipient, string $body): void
    {
        $email = (new Email())
            ->from('mysender@example.com')
            ->to($recipient)
            ->subject($subject)
            ->text($body);

        $this->mailer->send($email);
    }
}